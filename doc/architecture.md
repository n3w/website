# Systemarchitektur für <https://ftcommunity.de>

Für alle Lösungen wird Open-Source-Software genutzt.

## Lösung A: Verwendung eines dynamischen Content-Management-Systems

Ein _dynamisches_ oder _klassisches Content-Management-System (CMS)_ ist ein Programm,
das auf dem Server läuft und dort
dynamisch Webseiten generiert,
die dann zum Beispiel von den Rechten des angemeldeten Benutzers abhängen.
Die Daten dafür sind in einer Datenbank abgelegt.
Beispiel: Drupal, in der Programmiersprache PHP geschrieben.

### Vorteile
* Ein CMS kann wahrscheinlich alle Anforderungen (Benutzerverwaltung, Rechteverwaltung, Bilderpool) umsetzen.
* Die Absicherung erfolgt durch Sicherheitsupdates, die von den Entwicklern des CMS bereitgestellt werden.
* Die Anpassung an eigene Vorstellungen erfolgt durch die Konfiguration diverser Parameter.

### Nachteile
* Die Installation und Einrichtung ist abhängig von der Mächtigkeit des CMS nicht trivial.
* Ein großes CMS ist ein komplexes Programm mit einer unübersichtlichen Architektur.
* Durch die dynamische Seitengenerierung, den Einsatz von PHP und einer Datenbank 
  ergeben sich viele Möglichkeiten für Sicherheitslücken.
* Für die Sicherheitsupdates ist man auf die Entwickler-Community des CMS angewiesen.

 
## Lösung B: Komplette eigene Programmierung mit einem Webframework (hier: Ruby on Rails)

### Vorteile
* Man ist nicht so festgelegt wie bei einem CMS
* Anforderungen können schnell implementiert werden
* Klare Trennung von Logik und Darstelung durch Model-View-Controller Architektur
* Konvention vor Konfiguration bedeutet schnelle Einarbeitung durch neue Entwickler, hohe Wartbarkeit und übersichtliche Architektur auch bei großen Projekten. Es ist wenig Dokumentation erforderlich.
* Sicherheitslücken werden durch Konventionen des Frameworks und langjährige Reifung der verwendeten Module vermieden.
* Versorgung von Sicherheitsupdates durch das Framework und verwendeten Module
* Integration von Unit- und Integrationstests Bestandteil der Architektur

### Nachteile
* Updates auf neue Major-Versionen sehr aufwendig, siehe z.B. [github](https://githubengineering.com/upgrading-github-from-rails-3-2-to-5-2/)
* Installation und Konfiguration nicht trivial, hier ist Unterstützung durch einen Entwickler erforderlich. Der sollte zwangsläufig vorhanden sein..
* Einzelne, verwendete Module (gems) werden u.U. im Laufe der Zeit von den jeweiligen Entwicklern nicht mehr unterstützt. Hier sind dann umfangreiche Umbaumaßnahmen erforderlich
* Derzeit nicht auf Windows-Servern lauffähig
* Langsame Interpretersprache
* Kenntnisse in mehreren Programmiersprachen (Ruby, JavaScript) erforderlich
* Nur Backend, zusätzliche JavaScript Frameworks wie z.B. jQuery und Vue.js im Frontend erforderlich

### Mögliche Module
* [Upload von Dateien: carrierwave](https://github.com/carrierwaveuploader/carrierwave)
* [Thumpnail Generation: minimagick](https://github.com/minimagick/minimagick)
* [Benutzerverwaltung: devise](https://github.com/plataformatec/devise)
* [CSS Framework Bootstrap](https://github.com/twbs/bootstrap-sass)

## Lösung C: Komplette eigene Programmierung mit einem modernem Webframework z.B AngularJs, React oder Vuejs

### Vorteile
* Modernes Framework
* Vollständig im Frontend, Backend stellt nur die Datenbank zur Verfügung
