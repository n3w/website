# Anforderungen für <https://ftcommunity.de>

* Startseite: Zeigt bei jedem Zugriff eine andere Auswahl von Bildern
  aus dem Bilderpool an.
* Bilderpool
  * Erlaubt Suche nach Bildern
  * angemeldete und freigeschaltete Benutzer können
    * Bilder hochladen
	* Kommentare zu Bildern schreiben
* Admins/Redakteure können die Website editieren
* Es gibt eine Benutzerverwaltung mit
  * Login/Logout
  * (Selbst-)Registrierung von Benutzern
  * (Selbst-)Verwaltung von Profildaten durch angemeldete Benutzer
  * Vergabe/Verwalten von weitergehenden Rechten durch Admins
    * Schreibrecht im Bilderpool
    * Schreibrecht für die ganze Website ("Redakteur")
    * Zugang zur Rechteverwaltung ("Admin")
  * In einer späteren Ausbaustufe soll das [Forum](https://forum.ftcommunity.de) 
    dieselbe Benutzerverwaltung mit nutzen können ("Single Sign On")

# Zusätzliche Anforderung
* Übernahme von Daten aus dem alten System
    * Bilder
    * Texte
    * Benutzerdaten
